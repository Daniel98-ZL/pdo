<?php



require "../conexion1.php";

require "../common.php";



$success = null;



if (isset($_POST["submit"])) {

   if (!hash_equals($_SESSION['csrf'], $_POST['csrf']))

     die();



   try {

     $conexion = new PDO($dsn, $usuario, $contraseña);



    



     $sql = "DELETE FROM usuarios WHERE id = :id";



     $statement = $conexion->prepare($sql);

     $statement->bindValue(':id', $id);

     $statement->execute();



     $success = "Usuario eliminado correctamente";

  } catch (PDOException $error) {

     echo $sql . "<br>" . $error->getMessage();

  }

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  <title>Account Login</title>
</head>
<body class="bg-primary">
<div class="container">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card card-body bg-light mt-5">
      <h2>Login</h2>
      <p>Logueado.</p>
      <form action="" method="post">
          <div class="form-group">
              <label>Email:<sup>*</sup></label>
              <input type="text" name="email" class="form-control form-control-lg" value="">
              <span class="invalid-feedback"></span>
          </div>    
          <div class="form-group">
              <label>Password:<sup>*</sup></label>
              <input type="contraseña" name="contraseña" class="form-control form-control-lg">
              <span class="invalid-feedback"></span>
          </div>
          <div class="form-row">
            <div class="col">
              <input type="submit" class="btn btn-success btn-block" value="Login">
            </div>
            <div class="col">
              <a href="registro.php" class="btn btn-light btn-block">No account? Registro</a>
            </div>
        </div>
      </form>
        </div>
      </div>
    </div>
</div>    
</body>
</html>