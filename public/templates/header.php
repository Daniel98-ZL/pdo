<!doctype html>
<html lang="es">

<head>
 <meta charset="utf-8">
 <meta http-equiv="x-ua-compatible" content="ie=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <title>PHP - PDO </title>

 <link rel="stylesheet" href="css/style.css">
 <style>
    h2 {
        background-color: #ccc;
        text-align: center;
        color: black;
    }
    body {
        background-color: #FFC300;
    }
    form {
        color: black;
        width:300px;
        padding:16px;
        border-radius:10px;
        margin:auto;
        background-color:#ccc;
    }
    form label{
        width:72px;
        font-weight:bold;
        display: inline-table;
    }
    form input{
        width:100%;
        padding:8px 16px;
        margin-top:32px;
        border:1px solid #000;
        border-radius:5px;
        display:block;
        color:black;
        background-color:whitesmoke;
    } 
    form input[type="submit"]:hover{
        cursor:pointer;
        color: white;
        background-color: #000;
    }
    a {
        outline: none;
        text-decoration: none;
        padding: 2px 1px 0;
        }

        a:link {
        color: #265301;
        }

        a:visited {
        color: #437A16;
        }

        a:focus {
        border-bottom: 1px solid;
        background: #BAE498;
        }

        a:hover {
        border-bottom: 1px solid;
        background: #CDFEAA;
        }

        a:active {
        background: #265301;
        color: #CDFEAA;
        }
 </style>
</head>

<body>
 <h1>CRUD PHP - PDO</h1>
